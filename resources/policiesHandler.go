package resources

import (
	"github.com/vx-labs/identity-types/server"
	"github.com/vx-labs/go-rest-api"
	"github.com/vx-labs/authorization/services"
	"github.com/vx-labs/identity-api/store"
	"github.com/vx-labs/identity-api/index"
	"github.com/vx-labs/identity-types/authorization"
)

type policiesHandler struct {
	state    server.StateStore
	events   chan server.Event
	commands chan server.Event
}

func PoliciesHandler() server.ResourceHandler {
	p := &policiesHandler{
		events:   make(chan server.Event),
		commands: make(chan server.Event),
	}
	p.state = store.NewMemoryStore(p.Indexes())
	for _, pol := range services.DefaultPolicies {
		p.state.Insert(pol)
	}
	return p
}

func (h *policiesHandler) Schema() server.Resource {
	return &authorization.Policy{}
}
func (h *policiesHandler) Store() server.StateStore {
	return h.state
}
func (h *policiesHandler) Indexes() map[string]server.ResourceIndex {
	return index.IdOnly().Add(server.ResourceIndex{
		Id:     "name",
		Unique: false,
		Field:  "Name",
	}).Build()

}
func (h *policiesHandler) Events() chan server.Event {
	return h.events
}
func (h *policiesHandler) Commands() chan server.Event {
	return h.commands
}

func (h *policiesHandler) EventsSubscriptions() map[string]server.EventHandler {
	return map[string]server.EventHandler{}
}
func (h *policiesHandler) CommandsSubscriptions() map[string]server.EventHandler {
	return map[string]server.EventHandler{}
}
func (h *policiesHandler) Grammar() api.Grammar {
	return api.Grammar{
		Plural:   "policies",
		Singular: "policy",
	}
}
