package main

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/vx-labs/go-rest-api"
	clientAPI "github.com/vx-labs/go-rest-api/client"
	authentication "github.com/vx-labs/identity-api/authentication"
	policies "github.com/vx-labs/identity-api/authorization"
	types "github.com/vx-labs/identity-types/authorization"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
)

func boot(role, secret, endpoint string) {
	if role != "" {
		os.Setenv("APPROLE_ID", role)
	}
	if secret != "" {
		os.Setenv("APPROLE_SECRET", secret)
	}
	if endpoint != "" {
		os.Setenv("AUTHORIZATION_ENDPOINT", endpoint)
	}
}

func main() {
	ctx := context.Background()
	logger := clientAPI.NewLogger()
	logger.SetLevel(logrus.ErrorLevel)

	app := kingpin.New("policies", "vx policies api client")
	role := app.Flag("role", "identity API role").String()
	secret := app.Flag("secret", "identity API secret").String()
	endpoint := app.Flag("endpoint", "authorization API endpoint").Default("https://authorization.cloud.vx-labs.net/v1").String()

	appReq := app.Command("match", "policies test tool")
	appReqPolicyId := appReq.Flag("policy-id", "policy id").Short('p').Required().String()
	appReqVerb := appReq.Flag("verb", "http verb").Short('v').Default("get").String()
	appReqService := appReq.Flag("service", "service").Short('s').Required().String()
	appReqResource := appReq.Flag("resource", "resource").Short('r').Required().String()
	appReqInstance := appReq.Flag("instance", "resource").Short('i').Required().String()

	appPol := app.Command("policies", "policies management")
	appPolList := appPol.Command("list", "list policies").Alias("ls")
	appPolListFull := appPolList.Flag("full", "include details").Default("false").Short('f').Bool()
	appPolDel := appPol.Command("rm", "delete policy").Alias("delete")
	appPolDelId := appPolDel.Flag("instance", "policy to delete").Short('i').String()

	appPolCreate := appPol.Command("create", "create policy").Alias("new")
	appPolCreateName := appPolCreate.Flag("name", "policy name").Short('n').String()
	appPolCreateRule := appPolCreate.Flag("rule", "policy rule").Short('r').Strings()
	appPolCreateDesc := appPolCreate.Flag("description", "policy description").Short('d').String()

	appPolUpdate := appPol.Command("update", "update policy")
	appPolUpdateName := appPolUpdate.Flag("name", "policy name").Short('n').String()
	appPolUpdateDesc := appPolUpdate.Flag("description", "policy description").Short('d').String()
	appPolUpdateId := appPolUpdate.Flag("id", "policy id").Short('i').String()

	appPolAppend := appPol.Command("append", "append rules to a policy")
	appPolAppendRules := appPolAppend.Flag("rules", "policy rule").Short('r').Strings()
	appPolAppendId := appPolAppend.Flag("id", "policy id").Short('i').String()
	appPolAppendFlush := appPolAppend.Flag("flush", "remove the other rules from policy").Short('f').Bool()

	cmd := kingpin.MustParse(app.Parse(os.Args[1:]))
	boot(*role, *secret, *endpoint)
	auth, err := authentication.NewClient(ctx, logger.WithField("source", "authentication_client"))
	if err != nil {
		logger.Fatal(err.Error())
	}
	a, err := policies.NewClient(ctx, logger.WithField("source", "authorization_client"))
	if err != nil {
		logger.Fatal(err.Error())
	}
	a.WithJWTProvider(auth)

	switch cmd {
	case appReq.FullCommand():
		appReqMatch(ctx, logger, a, *appReqPolicyId, *appReqService, *appReqResource, *appReqInstance, *appReqVerb)
	case appPolList.FullCommand():
		polList(ctx, logger, a, *appPolListFull)
	case appPolDel.FullCommand():
		a.Policies().Delete(ctx, *appPolDelId)
	case appPolAppend.FullCommand():
		p, err := a.Policies().ById(ctx, *appPolAppendId)
		if err != nil {
			logger.Fatal(err.Error())
		}
		if *appPolAppendFlush {
			p.Rules = []types.Rule{}
		}
		for _, r := range *appPolAppendRules {
			parsedRule := types.Rule{}
			if parsedRule.UnmarshalString(r) == nil {
				p.Rules = append(p.Rules, parsedRule)
			} else {
				logger.Fatalf("invalid policy: %s", r)
			}
		}
		a.Policies().Update(ctx, p)
	case appPolUpdate.FullCommand():
		p, err := a.Policies().ById(ctx, *appPolUpdateId)
		if err != nil {
			logger.Fatal(err.Error())
		}
		if *appPolUpdateDesc != "" {
			p.Description = *appPolUpdateDesc
		}
		if *appPolUpdateName != "" {
			p.Name = *appPolUpdateName
		}
		a.Policies().Update(ctx, p)
	case appPolCreate.FullCommand():
		p := a.Policies().New().
			WithName(*appPolCreateName).
			WithStringRules(*appPolCreateRule).
			WithDescription(*appPolCreateDesc)
		a.Policies().Create(ctx, p)
	}
}

func polList(ctx context.Context, logger *logrus.Logger, a policies.Client, full bool) {
	l, err := a.Policies().List(ctx)
	if err != nil {
		logger.Fatal(err.Error())
	}
	fmt.Println("Policies:")
	for _, pol := range l {
		fmt.Printf("\n")
		fmt.Printf("  * %s\n", pol)
		if full {
			details, err := a.Policies().ById(ctx, pol)
			if err == nil {
				fmt.Printf("    Name: %s\n", details.Name)
				fmt.Printf("    Creation date: %s\n", details.Created.String())
				fmt.Printf("    Last update date: %s\n", details.Updated.String())
				fmt.Printf("    Description: %s\n", details.Description)
				fmt.Println("    Rules:")
				for _, rule := range details.Rules {
					fmt.Printf("      * %s\n", rule.ToString())
				}
			}
		}
	}
}

func appReqMatch(ctx context.Context, logger *logrus.Logger, a policies.Client, policy, service, resource, instance, method string) {
	verb := api.READ
	switch method {
	case "read":
		verb = api.READ
	case "create":
		verb = api.CREATE
	case "list":
		verb = api.LIST
	case "delete":
		verb = api.DELETE
	case "update":
		verb = api.UPDATE
	}

	grant := a.Match(ctx, types.Request{
		Verb:     verb,
		Instance: instance,
		Resource: resource,
		Service:  service,
		PolicyId: policy,
	})
	if grant {
		fmt.Println("access granted")
	} else {
		fmt.Println("access denied")
	}
}
