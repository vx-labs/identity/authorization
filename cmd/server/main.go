package main

import (
	"context"
	"fmt"
	api "github.com/vx-labs/go-rest-api/client"
	"github.com/vx-labs/identity-api/authentication"
	"os"
	"github.com/vx-labs/authorization/resources"
)

func main() {
	fmt.Println("starting authorization service")

	logger := api.NewLogger()
	ctx := context.Background()
	auth, err := authentication.NewClient(ctx, logger.WithField("source", "identity_client"))
	if err != nil {
		logger.Errorf("could not start identity API: %s", err.Error())
		os.Exit(1)
	}
	server, err := auth.NewResourceServer(ctx, logger.WithField("source", "service"), "identity", "v1")
	server.AddResource("/", resources.PoliciesHandler())
	server.ListenAndServe(ctx, 8007)
}
