package services

import (
	"github.com/vx-labs/go-rest-api"
	types "github.com/vx-labs/identity-types/authorization"
)

var RootPolicy = &types.Policy{
	Id: "00000000-0000-4000-0000-000000000000",
	Rules: []types.Rule{
		{Resource: "*", Grant: types.GrantRoot, Instance: "*", Service: "*"},
	},
	Name:        "_root",
	Tags:        []string{"default"},
	Description: "Root policy allows accessing every resources",
}

var AuthenticationPolicy = &types.Policy{
	Id: "00000000-0000-4000-0000-000000000001",
	Rules: []types.Rule{
		{Resource: "*", Grant: types.Grant{api.READ: true}, Instance: "*", Service: "identity"},
	},
	Name: "_authentication",
	Tags: []string{"default"},
	Description: "Authentication policy allows accessing essentials identity " +
		"services, and is used by the Authentication service",
}

var DefaultPolicies = []*types.Policy{
	RootPolicy,
	AuthenticationPolicy,
}
